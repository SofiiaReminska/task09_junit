package com.epam.lab.task09.testsuite;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(JUnitPlatform.class)
@Suite.SuiteClasses({ClassPositiveTest.class, ClassNegativeTest.class})
public class TestSuite {

}
