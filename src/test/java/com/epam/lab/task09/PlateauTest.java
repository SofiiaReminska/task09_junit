package com.epam.lab.task09;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class PlateauTest {
    @Spy
    private Plateau unit;

    public static final String PRINT_LARGEST_PLATEAU_POSITIVE_RESULT = "Biggest plateau starts at index: 2 and has length: 3";
    public static final String PRINT_LARGEST_PLATEAU_NEGATIVE_RESULT = "No plateau";

    @Test
    public void printLargestPlateauNegativeTest() {
        assertEquals(PRINT_LARGEST_PLATEAU_NEGATIVE_RESULT, unit.printLargestPlateau(-1, 3));
    }

    @Test
    public void printLargestPlateauPositiveTest() {
        assertEquals(PRINT_LARGEST_PLATEAU_POSITIVE_RESULT, unit.printLargestPlateau(2, 3));
    }

    @Test
    public void isStartOfPlateauPositiveTest() {
        assertTrue(unit.isStartOfPlateau(1, new int[]{1, 2, 3}));
    }

    @Test
    public void isStartOfPlateauNegativeTest() {
        assertFalse(unit.isStartOfPlateau(-1, new int[]{1, 2, 3}));
        assertFalse(unit.isStartOfPlateau(1, new int[]{2, 2, 3}));
    }

    @Test
    public void isEndOfPlateauPositiveTest() {
        assertTrue(unit.isEndOfPlateau(1, new int[]{2, 1, 1}));
    }

    @Test
    public void isEndOfPlateauNegativeTest() {
        assertFalse(unit.isEndOfPlateau(1, new int[]{1, 1, 1}));
    }

    @Test
    public void getLargestPlateauTest() {
        unit.getLargestPlateau(new int[]{1, 2, 2, 2, 1});
        Mockito.verify(unit).printLargestPlateau(1, 3);
    }
}
