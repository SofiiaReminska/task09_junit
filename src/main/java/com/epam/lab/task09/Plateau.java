package com.epam.lab.task09;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Plateau {
    private static final Logger LOGGER = LogManager.getLogger(Plateau.class);

    public void getLargestPlateau(int[] values) {
        int biggestStartIndex = -1;
        int biggestLenth = 0;
        int currentIndex = 1;
        int currentPlateauStartIndex = 1;
        int currentLength = 1;
        boolean plateauStarted = false;
        while (currentIndex < values.length) {
            if (isStartOfPlateau(currentIndex, values)) {
                plateauStarted = true;
                currentPlateauStartIndex = currentIndex;
            } else if (isEndOfPlateau(currentIndex, values)) {
                if (plateauStarted && currentLength > biggestLenth) {
                    biggestLenth = currentLength;
                    biggestStartIndex = currentPlateauStartIndex;
                }
                plateauStarted = false;
                currentLength = 1;
            } else {
                currentLength++;
            }
            currentIndex++;
        }
        LOGGER.info(printLargestPlateau(biggestStartIndex, biggestLenth));
    }

    public String printLargestPlateau(int biggestStartIndex, int biggestLength) {
        if (biggestStartIndex < 0) {
            return "No plateau";
        } else {
            return String.format("Biggest plateau starts at index: %d and has length: %d", biggestStartIndex, biggestLength);
        }
    }

    public boolean isStartOfPlateau(int index, int[] values) {
        if (index <= 0) {
            return false;
        }
        return values[index - 1] < values[index];
    }

    public boolean isEndOfPlateau(int index, int[] values) {
        if (index <= 0) {
            return false;
        }
        return values[index - 1] > values[index];
    }
}
